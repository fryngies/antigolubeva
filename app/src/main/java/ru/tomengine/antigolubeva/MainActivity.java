package ru.tomengine.antigolubeva;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText editArr;
    private EditText editRes;
    private Sort sort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editArr = (EditText) findViewById(R.id.arrEdit);
        editRes = (EditText) findViewById(R.id.editTextResult);
        sort = new Sort();
    }

    public int[] getArrEdit() {
        if(editArr.getText().length() == 0)
            return new int[0];

        String[] strArr = editArr.getText().toString().split(" ");
        int[] arr = new int[strArr.length];
        for(int i = 0; i < strArr.length; ++i)
            arr[i] = Integer.parseInt(strArr[i]);

        return arr;
    }

    public void pasteClean() {
        editRes.setText("");
    }

    private void appendText(String text) {
        editRes.setText(editRes.getText() + text);
    }

    private void appendList(String title, List<String> l) {
        appendText("[" + title + "]:\n");
        for(String el : l) {
            appendText("\t" + el + "\n");
        }
    }

    public void pasteSort() {
        appendList(getString(R.string.shell_sort), sort.Shell());
        appendText("\n");
        appendList(getString(R.string.heap_sort), sort.Heap());
        for(int i = 0; i < 25; ++i)
            appendText("-");
        appendText("\n");
    }

    public void onSubmit(View view) {
        int[] arr = getArrEdit();
        if(arr.length == 0)
            return;
        sort.setArr(arr);
        pasteSort();
    }

    public void onClean(View view) {
        pasteClean();
    }
}
