package ru.tomengine.antigolubeva;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Sort {
    private static int[] arr;
    private int[] marr;
    protected List<String> res;

    Sort() {}

    public Sort(int[] narr) {
        arr = narr;
    }

    int[] setArr(int[] narr) { return arr = narr; }

    public int[] getArr() { return arr; }

    void swap(int i, int j) {
        res.add(String.format("swapping %d and %d", i, j));
        int tmp = marr[i];
        marr[i] = marr[j];
        marr[j] = tmp;
    }

    String arrToString(int[] a) {
        String nstr = "[";

        nstr += String.format("%d(%d)", a[0], 0);
        for(int i = 1; i < a.length; ++i)
            nstr += String.format(", %d(%d)", a[i], i);
        nstr += "]";

        return nstr;
    }

    public List<String> Shell() {
        res = new ArrayList<>();
        marr = arr.clone();
        int m = marr.length;

        for(int k = m/2; k > 0; k /= 2) {
            res.add(String.format("k = %d:", k));
            res.add("\t" + arrToString(marr));
            for(int i = k; i < m; ++i) {
                int buf = marr[i];
                int j;
                for (j = i; j >= k && buf < marr[j - k]; j -= k) {
                    marr[j] = marr[j - k];
                    res.add("\t" + arrToString(marr));
                }

                marr[j] = buf;
                if(i != j) {
                    res.add("\t" + arrToString(marr));
                    res.add(String.format("%d moved to %d", i, j));
                    res.add("\n");
                }
            }
        }

        res.add(Arrays.toString(marr));

        return res;
    }

    private void HeapInsert(int i, int n) {
        while (2 * i + 1 < n) {
            int left = 2 * i + 1;
            int right = 2 * i + 2;
            int j = right < n && marr[right] > marr[left] ? right : left;

            if(marr[i] >= marr[j])
                break;
            swap(i, j);
            res.add("\t" + arrToString(marr));
            i = j;
        }
    }

    public List<String> Heap() {
        res = new ArrayList<>();
        marr = arr.clone();
        int m = marr.length;

        res.add(arrToString(marr));
        res.add("Building heap:");
        for (int i = (m-1)/2; i >= 0; --i) {
            res.add(String.format("--for %d", i));
            HeapInsert(i, m);
        }

        res.add("Sorting:");
        for (int i = m-1; i > 0; --i) {
            res.add(String.format("--for %d", i));
            swap(i, 0);
            HeapInsert(0, i);
        }

        res.add(arrToString(marr));

        return res;
    }
}
